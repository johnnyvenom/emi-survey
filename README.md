The Electronic Musical Instrument Survey
========================================

**A questionnaire on the use of technology in live performance**

(Formerly online at: https://emisurvey.online)

An online questionnaire was administered in 2018 to survey musicians about their use of new electronic musical instruments (EMIs) and technologies in performance.

A preliminary paper on the survey was published at the 2019 International Symposium on Computer Music Multidisciplinary Research (CMMR) in Marseille, France. [link](http://www-new.idmil.org/publication/surveying-digital-musical-instrument-use-across-diverse-communities-of-practice/)

The survey is the subject of an article being prepared for submission to the *Journal of New Music Research*, and will also appear as Chapter 2 of my Ph.D. thesis, which will be submitted in December 2020. 

This repository contains the following accompanying documents: 

1. [Survey questionnaire](EMI_survey_questionnaire.pdf)
2. [Thematic analysis codebook 1: Instrument descriptions and functionality](codebook-instrument-qualities.pdf)
3. [Thematic analysis codebook 2: User engagement](codebook-engagement.pdf)
4. [Crosstabulation analysis: results](crosstab-analysis.pdf)

